import React from 'react';
import {Breadcrumb, BreadcrumbItem, Button, Col, Container, FormGroup, Modal, ModalBody, Row} from "reactstrap";
import {Link, withRouter} from "react-router-dom";
import {Popconfirm, Table, Tag} from "antd";
import categoriesAction from "../../store/categories/action";
import '../../scss/_custome-tables.scss';
import {AvField, AvForm} from "availity-reactstrap-validation";
import {connect} from 'react-redux';
import {activateAuthLayout} from "../../store/layout/actions";

const {loadAllCategoriesRequested} = categoriesAction;

function removeBodyCss() {
    document.body.classList.add('no_padding');
}

class Categories extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            selectedCategory: {
                id: '',
                category: '',
                level: 1,
                parentCategory: null
            },
            isUpdate: false,
            parentCategories: []
        };
        this.onFormModalChanges = this.onFormModalChanges.bind(this);
        this.onAddCategory = this.onAddCategory.bind(this);
        this.onFormModalSubmit = this.onFormModalSubmit.bind(this);
    }

    componentDidMount() {
        this.props.activateAuthLayout();
        this.props.loadAllCategoriesRequested();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.categories !== this.props.categories) {
            const parentCategories = [];
            this.props.categories.forEach(category => {
                if (category.level === 1) {
                    parentCategories.push(<option key={category.id} value={category.id}>{category.category}</option>);
                }
            });
            this.setState({
                parentCategories: parentCategories
            });
        }
    }

    editCategory(category) {
        this.setState({
            showModal: true,
            selectedCategory: category,
            isUpdate: true,
        });
        removeBodyCss();
    };

    onAddCategory() {
        this.setState({
            showModal: true,
            selectedCategory: {
                id: '',
                category: '',
                level: 1,
                parentCategory: null
            },
            isUpdate: false,
        });
        removeBodyCss();
    }

    onFormModalChanges(e) {
        e.persist();
        const target = e.target;
        const selectedCategory = this.state.selectedCategory;

        // catch field level to convert value as number
        if (target.name === 'level') {
            selectedCategory['level'] = +target.value;
        } else {
            selectedCategory[target.name] = target.value;
        }
        // Set parent id as null if level 1
        if (selectedCategory.level === 1) {
            selectedCategory['parentCategory'] = null;
        }
        this.setState({
            selectedCategory: selectedCategory
        });
    }

    onFormModalSubmit() {
        console.log(this.state.selectedCategory);
    }

    render() {
        const columns = [
            {
                title: 'Category',
                dataIndex: 'category',
                key: 'category',
                width: '360px',
                sorter: (a, b) => {
                    if (a.title < b.title) return -1;
                    if (a.title > b.title) return 1;
                    return 0;
                },
                render: (text, row) => {
                    return <p>{row.category}</p>;
                },
            },
            {
                title: 'Level',
                dataIndex: 'level',
                key: 'level',
                width: '150px',
                render: (text, row) => {
                    return (<Tag>{row.level}</Tag>);
                },
            },
            {
                title: 'Actions',
                key: 'action',
                width: '100px',
                className: 'noWrapCell',
                render: (text, row) => {
                    return (
                        <div className="action-wrapper">
                            <a href="javascript:void(0)" onClick={() => this.editCategory(row)}>
                                <i className="ti-pencil"/>
                            </a>
                            <Popconfirm
                                title="Are you sure to delete this article？"
                                okText="Yes"
                                cancelText="No"
                                placement="topRight"
                            >
                                <a className="deleteBtn" href="javascript:void(0)">
                                    <i className="ti-close"/>
                                </a>
                            </Popconfirm>
                        </div>
                    );
                },
            },
        ];
        return (
            <React.Fragment>
                <Container fluid>
                    <div className="page-title-box">
                        <Row className="align-items-center">
                            <Col sm="6">
                                <h4 className="page-title">Categories</h4>
                                <Breadcrumb>
                                    <BreadcrumbItem><Link to="/">Rabbit Library</Link></BreadcrumbItem>
                                    <BreadcrumbItem
                                        active>Categories</BreadcrumbItem>
                                </Breadcrumb>
                            </Col>
                            <Col sm="6">
                                <div className="float-right d-none d-md-block">
                                    <Button onClick={this.onAddCategory} color="primary">Add Category</Button>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg="12">
                                <div className="ant-table-responsive">
                                    <Table dataSource={this.props.categories} columns={columns}
                                           loading={this.props.isLoading} rowKey="id"/>
                                </div>
                            </Col>
                        </Row>
                        <Modal isOpen={this.state.showModal} toggle='center'>
                            <div className="modal-header">
                                <h5 className="modal-title mt-0">{this.state.isUpdate ? 'Update Category' : 'Create Category'}</h5>
                                <button type="button" onClick={() => this.setState({showModal: false})}
                                        className="close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <ModalBody>
                                <AvForm>
                                    <AvField name="category" label="Category"
                                             placeholder="Category Title" type="text"
                                             errorMessage="Category title is required"
                                             onChange={this.onFormModalChanges}
                                             value={this.state.selectedCategory.category}
                                             validate={{required: {value: true}}}/>
                                    <AvField type="select" name="level"
                                             onChange={this.onFormModalChanges}
                                             value={this.state.selectedCategory.level}
                                             label="Level">
                                        <option value={1}>1</option>
                                        <option value={2}>2</option>
                                    </AvField>
                                    {this.state.selectedCategory.level === 2 &&
                                    <AvField type="select" name="parentCategory"
                                             onChange={this.onFormModalChanges}
                                             value={this.state.selectedCategory.parentCategory} label="Parent Category">
                                        {this.state.parentCategories}
                                    </AvField>}
                                    <FormGroup className="mb-0">
                                        <div>
                                            <Button type="button" color="primary"
                                                    onClick={this.onFormModalSubmit}
                                                    className="mr-1">
                                                Submit
                                            </Button>
                                            <Button type="button"
                                                    onClick={() => this.setState({showModal: false})}
                                                    color="secondary">
                                                Cancel
                                            </Button>
                                        </div>
                                    </FormGroup>
                                </AvForm>
                            </ModalBody>
                        </Modal>
                    </div>
                </Container>
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    const {categories, isLoading} = state.Categories;
    return {isLoading, categories};
};

export default withRouter(connect(mapStateToProps, {activateAuthLayout, loadAllCategoriesRequested})(Categories));
