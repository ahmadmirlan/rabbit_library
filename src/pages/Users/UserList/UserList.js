import React, {useEffect, useState, Fragment} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import userAction from '../../../store/users/action';
import {activateAuthLayout} from "../../../store/layout/actions";
import {Breadcrumb, BreadcrumbItem, Button, Col, Container, Row} from "reactstrap";
import {Link} from "react-router-dom";
import {Popconfirm, Table, Tag} from 'antd';
import '../../../scss/_custome-tables.scss';

const {loadAllUseRequested} = userAction;

export default function UserList() {
    const [state, setState] = useState({
        query: {
            filter: {},
            pageNumber: 0,
            pageSize: 3,
            sortField: "createdAt",
            sortOrder: "desc"
        }
    });

    const {isLoading, users, lastQuery, pages} = useSelector(state => state.Users);
    const {roles} = useSelector(state => state.Roles);

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(activateAuthLayout());
        dispatch(loadAllUseRequested(state.query));
    }, [dispatch]);

    const columns = [
        {
            title: 'First Name',
            dataIndex: 'firstName',
            key: 'firstName',
            width: '300px',
            sorter: () => {
            },
            render: (text, row) => {

                return <p>{row.firstName}</p>;
            },
        },
        {
            title: 'Last Name',
            dataIndex: 'lastName',
            key: 'lastName',
            width: '300px',
            sorter: () => {
            },
            render: (text, row) => {

                return <p>{row.lastName}</p>;
            },
        },
        {
            title: 'Username',
            dataIndex: 'username',
            key: 'username',
            width: '150px',
            sorter: () => {
            },
            render: (text, row) => {

                return <p>@{row.username}</p>;
            },
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
            width: '250px',
            sorter: () => {
            },
            render: (text, row) => {
                return <p>{row.email}</p>;
            },
        },
        {
            title: 'Roles',
            dataIndex: 'roles',
            key: 'roles',
            width: '250px',
            sorter: () => {
            },
            render: (text, row) => {
                const findRoleName = roleId => {
                  const role = roles.find(role => {
                      return role.id === roleId;
                  });
                  if (role) {
                      switch (role.title) {
                          case 'Admin':
                              return <Tag color="red">{role.title}</Tag>;
                          case 'User':
                              return <Tag color="green">{role.title}</Tag>;
                          default:
                              return <Tag color="green">{role.title}</Tag>;
                      }
                  } else {
                      return '';
                  }
                };
                return findRoleName(row.roles[0]);
            },
        },
        {
            title: 'Actions',
            key: 'action',
            width: '100px',
            className: 'noWrapCell',
            render: (text, row) => {
                return (
                    <div className="action-wrapper">
                        <a href="#">
                            <i className="ti-pencil"/>
                        </a>
                        <Popconfirm
                            title="Are you sure to delete this article？"
                            okText="Yes"
                            cancelText="No"
                            placement="topRight"
                        >
                            <a className="deleteBtn" href="#">
                                <i className="ti-close"/>
                            </a>
                        </Popconfirm>
                    </div>
                );
            },
        },
    ];

    return (
        <Fragment>
            <Container fluid>
                <div className="page-title-box">
                    <Row className="align-items-center">
                        <Col sm="6">
                            <h4 className="page-title">User List</h4>
                            <Breadcrumb>
                                <BreadcrumbItem><Link to="/">Rabbit Library</Link></BreadcrumbItem>
                                <BreadcrumbItem
                                    active>User List</BreadcrumbItem>
                            </Breadcrumb>
                        </Col>
                        <Col sm="6">
                            <div className="float-right d-none d-md-block">
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Table dataSource={users} columns={columns} loading={isLoading}
                        />
                    </Row>
                </div>
            </Container>
        </Fragment>
    );
}
