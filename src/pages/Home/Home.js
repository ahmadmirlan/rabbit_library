import React from "react";
import {Card, CardBody, Col, Container, Row} from "reactstrap";
import {truncateHTML} from '../../helpers/utils';
import {Link, withRouter} from "react-router-dom";
import {connect} from 'react-redux';
import {Input, Pagination} from 'antd';
import {activateAuthLayout} from "../../store/layout/actions";
import actionBooks from '../../store/books/action';
import categoryActions from '../../store/categories/action';
import Loader from '../../components/Loader/loader';
import {Button} from 'antd';
import './Home.scss';

const {Search} = Input;
const {loadPublishedBookRequested} = actionBooks;
const {loadAllCategoriesRequested} = categoryActions;

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            query: {
                filter: {},
                pageNumber: 0,
                pageSize: 9,
                sortField: "createdAt",
                sortOrder: "desc"
            }
        };
        this.onPaginationChanges = this.onPaginationChanges.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    componentDidMount() {
        this.props.activateAuthLayout();
        this.props.loadPublishedBookRequested(this.state.query);
        this.props.loadAllCategoriesRequested();
    }

    // put async because we will access state value immediately
    async onPaginationChanges(page, pageSize) {
        await this.setState((prevState) => ({
            query: {
                filter: prevState.query.filter,
                pageNumber: page - 1,
                pageSize: pageSize,
                sortField: prevState.query.sortField,
                sortOrder: prevState.query.sortOrder
            }
        }));
        this.props.loadPublishedBookRequested(this.state.query);
    }

    async onSearch(value) {
        const query = this.state.query;
        query.filter['title'] = value;
        query.pageNumber = 0;
        await this.setState((prevState) => ({
            query: query
        }));
        this.props.loadPublishedBookRequested(this.state.query);
    }

    render() {
        return (
            <React.Fragment>
                <Container fluid>
                    <div className="page-title-box">
                        <Row className="align-items-center">
                            <Col lg="12" className="d-flex justify-content-center">
                                <Search
                                    placeholder="Search Book"
                                    onSearch={value => this.onSearch(value)}
                                    style={{width: 300}}
                                />
                            </Col>
                        </Row>
                    </div>
                    <Row>
                        {this.props.isLoading && <Loader/>}
                        {!this.props.isLoading && this.props.books.map(book => {
                            return <Col sm="12" md="6" lg="6" xl="4" key={book.id}>
                                <Card>
                                    <img className="card-img-top img-fluid image-book-cover" src={book.cover}
                                         alt={book.title}/>
                                    <CardBody>
                                        <h4 className="card-title font-16 mt-0">{book.title}</h4>
                                        <p className="card-text">{truncateHTML(book.body, 150)}</p>
                                        <Button type="primary" className="mr-3">Start Renting</Button>
                                        <Button type="dashed">Detail Book</Button>
                                    </CardBody>
                                </Card>
                            </Col>
                        })}
                    </Row>
                    <Row>
                        <Col col="12" className="d-flex justify-content-center">
                            {!this.props.isLoading && this.props.pages.totalPage > 1 &&
                            <Pagination simple
                                        defaultCurrent={this.state.query.pageNumber + 1}
                                        onChange={this.onPaginationChanges}
                                        total={this.props.pages.totalElements}
                                        defaultPageSize={this.state.query.pageSize}/>}
                        </Col>
                    </Row>
                </ Container>
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    const {books, isLoading, lastQuery, pages} = state.Books;
    const {categories} = state.Categories;
    return {isLoading, books, lastQuery, pages, categories};
};

export default withRouter(connect(mapStateToProps, {
    activateAuthLayout,
    loadPublishedBookRequested,
    loadAllCategoriesRequested
})(Home));
