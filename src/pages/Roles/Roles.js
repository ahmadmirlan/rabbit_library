import React from "react";
import {Breadcrumb, BreadcrumbItem, Col, Container, Row} from "reactstrap";
import {Button, Checkbox, Input, Modal, Popconfirm, Table, Tag} from 'antd';
import {Link, withRouter} from "react-router-dom";
import {activateAuthLayout} from "../../store/layout/actions";
import {connect} from 'react-redux';
import roleAction from "../../store/auth/roles/action";
import '../../scss/_custome-tables.scss';

const {loadAllRolesRequested} = roleAction;

class Roles extends React.Component {

   constructor(props) {
       super(props);
       this.state = {
           showModal: false,
           isUpdate: false,
           selectedRole: {},
           parentPermissions: [],
           childPermissions: [],
       };
       this.onOpenEditModal = this.onOpenEditModal.bind(this);
       this.onCloseModal = this.onCloseModal.bind(this);
       this.preparePermissions = this.preparePermissions.bind(this);
       this.onCheckedChange = this.onCheckedChange.bind(this);
       this.onChildCheckedChange = this.onChildCheckedChange.bind(this);
   }

   componentDidMount() {
       this.props.activateAuthLayout();
       this.props.loadAllRolesRequested();
   }

   async preparePermissions(permissions) {
       const childPermissions = this.props.permissions.filter(perm => perm.level === 2);
       const parentPermissions = this.props.permissions.filter(perm => perm.level === 1);

       childPermissions.map(perm => {
           const index = permissions.findIndex(rolePerm => {
               return rolePerm === perm.id;
           });
           perm['isSelected'] = index > -1;
           return perm;
       });

       parentPermissions.forEach(perm => {
           const index = permissions.findIndex(rolePerm => {
               return rolePerm === perm.id;
           });
           perm['isSelected'] = index > -1;
           perm['children'] = childPermissions.filter(childPerm => childPerm.parentId === perm.id);
       });
       await this.setState((prevState, prevProps) => ({
           ...prevState,
           parentPermissions: parentPermissions
       }));
       console.log(this.state.parentPermissions);
   }

    onOpenEditModal(role) {
       this.preparePermissions(role.permissions).then(() => {
           const prevState = this.state;
           prevState.showModal = true;
           prevState.isUpdate = true;
           prevState.selectedRole = role;
           this.setState(prevState);
       });
   }

   onCheckedChange(e, permissionId, index) {
       const permissions = this.state.parentPermissions;
       permissions[index].isSelected = e.target.checked;
       permissions[index].children.map(childPerm => {
           childPerm['isSelected'] = e.target.checked;
           return childPerm;
       });
       this.setState((prevState) => ({
           ...prevState,
           parentPermissions: permissions
       }));
   }

   onChildCheckedChange(e, childPermissions, parentIndex, childIndex) {
       const parentPermissions = this.state.parentPermissions;
       childPermissions[childIndex].isSelected = e.target.checked;
       parentPermissions[parentIndex].children = childPermissions;
       if (e.target.checked) {
           parentPermissions[parentIndex].isSelected = true;
       } else {
           const childPerm = parentPermissions[parentIndex].children.find(childPerm => {
               return childPerm.isSelected === true;
           });
           if (!childPerm) {
               parentPermissions[parentIndex].isSelected = false;
           }
       }
       this.setState((prevState) => ({
           ...prevState,
           parentPermissions: parentPermissions
       }));
   }

   onCloseModal() {
       const prevState = this.state;
       prevState.showModal = false;
       prevState.isUpdate = false;
       this.setState(prevState)
   }

    render() {
        const columns = [
            {
                title: 'Role',
                dataIndex: 'title',
                key: 'title',
                width: '300px',
                sorter: () => {
                },
                render: (text, row) => {

                    return <p>{row.title}</p>;
                },
            },
            {
                title: 'Is Core',
                dataIndex: 'isCoreRole',
                key: 'isCoreRole',
                width: '300px',
                sorter: () => {
                },
                render: (text, row) => {
                    const isCore = isCore => {
                        if (isCore) {
                            return <Tag color="red">Yes</Tag>
                        } else {
                            return <Tag color="green">No</Tag>
                        }
                    };
                    return isCore(row.isCoreRole);
                },
            },
            {
                title: 'Actions',
                key: 'action',
                width: '100px',
                className: 'noWrapCell',
                render: (text, row) => {
                    return (
                        <div className="action-wrapper">
                            <Button shape="circle" type="primary" icon={row.isCoreRole ? 'eye' : 'edit'} className="mr-1" onClick={() => this.onOpenEditModal(row)}/>
                            <Popconfirm
                                title="Are you sure to delete this article？"
                                okText="Yes"
                                cancelText="No"
                                placement="topRight"
                            >
                                <Button type="danger" shape="circle-outline" icon="delete"/>
                            </Popconfirm>
                        </div>
                    );
                },
            },
        ];
        return (
            <React.Fragment>
                <Container fluid>
                    <div className="page-title-box">
                        <Row className="align-items-center">
                            <Col sm="6">
                                <h4 className="page-title">Role List</h4>
                                <Breadcrumb>
                                    <BreadcrumbItem><Link to="/">Rabbit Library</Link></BreadcrumbItem>
                                    <BreadcrumbItem
                                        active>Role List</BreadcrumbItem>
                                </Breadcrumb>
                            </Col>
                            <Col sm="6">
                                <div className="float-right d-none d-md-block">
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg="12">
                                <Table columns={columns} loading={this.props.isLoading} dataSource={this.props.roles} rowKey='id' />
                            </Col>
                        </Row>
                        <Modal
                            visible={this.state.showModal}
                            title={this.state.isUpdate ? `Update Role ${this.state.selectedRole.title}` : 'Create Role'}
                            onCancel={this.onCloseModal}
                            footer={[
                                <Button key="back" onClick={this.onCloseModal}>
                                    Close
                                </Button>,
                                <Button key="submit" type="primary" disabled={this.state.selectedRole.isCoreRole}>
                                    Submit
                                </Button>,
                            ]}
                        >
                            <Row className="mb-3">
                                <Col lg="12">
                                    <Input placeholder="Title" value={this.state.selectedRole.title}/>
                                </Col>
                            </Row>
                            <Row>
                                {this.state.parentPermissions.map((parentPerm, index) => {
                                    return <Col lg="12 mb-3" key={parentPerm.id}>
                                        <div style={{ borderBottom: '1px solid #E9E9E9' }}>
                                            <Checkbox checked={parentPerm.isSelected} onChange={e => {this.onCheckedChange(e, parentPerm.id, index)}}>
                                                {parentPerm.title}
                                            </Checkbox>
                                        </div>
                                        <br />
                                        <div className="col-lg-10 offset-lg-2">
                                                {parentPerm.children.map((childPerm, childIndex) => {
                                                    return <React.Fragment key={childPerm.id}>
                                                        <Checkbox checked={childPerm.isSelected}
                                                                  onChange={e => this.onChildCheckedChange(e, parentPerm.children, index, childIndex)}>{childPerm.title}</Checkbox>
                                                    </React.Fragment>
                                                })}
                                        </div>
                                    </Col>
                                })}
                            </Row>
                        </Modal>
                    </div>
                </Container>
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    const {roles, permissions, isLoading} = state.Roles;
    return {roles, permissions, isLoading}
};

export default withRouter(connect(mapStateToProps, {activateAuthLayout, loadAllRolesRequested})(Roles));
