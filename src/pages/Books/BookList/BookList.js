import React, {Component} from "react";
import {Link, withRouter} from 'react-router-dom';
import {Breadcrumb, BreadcrumbItem, Col, Container, Row} from "reactstrap";
import Settingmenu from "../../Subpages/Settingmenu";
import {connect} from 'react-redux';
import {activateAuthLayout} from "../../../store/layout/actions";
import bookAction from '../../../store/books/action';
import categoryAction from '../../../store/categories/action';
import {Popconfirm, Table, Tag} from 'antd';
import './BookList.scss';

const {loadAllBookAdvancedRequested} = bookAction;
const {loadAllCategoriesRequested} = categoryAction;

class BookList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            query: {
                filter: {},
                pageNumber: 0,
                pageSize: 5,
                sortField: "createdAt",
                sortOrder: "desc"
            }
        };
        this.updateBook = this.updateBook.bind(this);
        this.onSortChanges = this.onSortChanges.bind(this);
    }

    componentDidMount() {
        this.props.activateAuthLayout();
        this.props.loadAllBookAdvancedRequested(this.state.query);
        this.props.loadAllCategoriesRequested();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.categories !== this.props.categories) {
            this.setState({
                categories: this.props.categories
            })
        }
    }

    updateBook(bookId) {
        this.props.history.push(`/books/manages/edit/${bookId}`)
    }

    async onSortChanges(pagination, filters, sorter) {
        const query = this.state.query;
        query.pageNumber = (pagination.current - 1);
        query.pageSize = pagination.pageSize;

        if (sorter && sorter.field) {
            query.sortField = sorter.field;
            query.sortOrder = sorter.order  === 'ascend' ? 'asc' : 'desc'
        }

        await this.setState({
            query: query
        });
        this.props.loadAllBookAdvancedRequested(this.state.query);
    }

    render() {
        const columns = [
            {
                title: 'Title',
                dataIndex: 'title',
                key: 'title',
                width: '360px',
                sorter: () => {
                },
                render: (text, row) => {
                    const trimByWord = sentence => {
                        let result = sentence;
                        let resultArray = result.split(' ');
                        if (resultArray.length > 20) {
                            resultArray = resultArray.slice(0, 20);
                            result = resultArray.join(' ') + '...';
                        }
                        return result;
                    };

                    return <p>{trimByWord(row.title)}</p>;
                },
            },
            {
                title: 'Authors',
                dataIndex: 'authors',
                key: 'authors',
                width: '220px',
                render: (text, row) => {
                    return (
                        <React.Fragment>
                            {row.authors.map((author, index) => {
                                return <Tag color="blue" key={index}>{author}</Tag>
                            })}
                        </React.Fragment>
                    );
                },
            },
            {
                title: 'Status',
                dataIndex: 'status',
                key: 'status',
                width: '150px',
                render: (text, row) => {
                    return (<Tag>{row.status}</Tag>);
                },
            },
            {
                title: 'Categories',
                dataIndex: 'categories',
                key: 'categories',
                width: '220px',
                render: (text, row) => {

                    const findCategoryName = (categoryId) => {
                        let catIndex = this.state.categories.findIndex(cat => {
                            return cat.id === categoryId;
                        });
                        if (catIndex > -1) {
                            return this.state.categories[catIndex].category;
                        } else {
                            return '';
                        }
                    };

                    return (
                        <React.Fragment>
                            {row.categories.map((cat, index) => {
                                return <Tag color='green' key={index}>{findCategoryName(cat)}</Tag>
                            })}
                        </React.Fragment>
                    );
                },
            },
            {
                title: 'Stock',
                dataIndex: 'stocks',
                key: 'stocks',
                width: '220px',
                sorter: () => {
                },
                render: (text, row) => {
                    return row.stocks;
                },
            },
            {
                title: 'Actions',
                key: 'action',
                width: '100px',
                className: 'noWrapCell',
                render: (text, row) => {
                    return (
                        <div className="action-wrapper">
                            <a href="#" onClick={() => this.updateBook(row.id)}>
                                <i className="ti-pencil"/>
                            </a>
                            <Popconfirm
                                title="Are you sure to delete this article？"
                                okText="Yes"
                                cancelText="No"
                                placement="topRight"
                            >
                                <a className="deleteBtn" href="#">
                                    <i className="ti-close"/>
                                </a>
                            </Popconfirm>
                        </div>
                    );
                },
            },
        ];

        return (
            <React.Fragment>
                <Container fluid>
                    <div className="page-title-box">
                        <Row className="align-items-center">
                            <Col sm="6">
                                <h4 className="page-title">Book</h4>
                                <Breadcrumb>
                                    <BreadcrumbItem><Link to="#">Rabbit Library</Link></BreadcrumbItem>
                                    <BreadcrumbItem><Link to="/books/manages/list">Books</Link></BreadcrumbItem>
                                </Breadcrumb>
                            </Col>
                            <Col sm="6">
                                <div className="float-right d-none d-md-block">
                                    <Settingmenu/>
                                </div>
                            </Col>
                        </Row>
                    </div>
                    <Row>
                        <Col lg="12">
                            <div className="ant-table-responsive">
                                <Table dataSource={this.props.books} columns={columns}
                                       loading={this.props.isLoading} rowKey="id"
                                       onChange={this.onSortChanges}
                                       pagination={{
                                           defaultPageSize: this.state.query.pageSize,
                                           showSizeChanger: true,
                                           defaultCurrent: this.state.query.pageNumber + 1,
                                           pageSizeOptions: ['3', '5', '10'],
                                           current: this.state.query.pageNumber + 1,
                                           total: this.props.pages.totalElements,
                                       }}
                                />
                            </div>
                        </Col>
                    </Row>
                </ Container>
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    const {isLoading, books, pages, lastQuery} = state.Books;
    const {categories} = state.Categories;
    return {isLoading, books, categories, pages, lastQuery};
};

export default withRouter(connect(mapStateToProps, {
    activateAuthLayout,
    loadAllBookAdvancedRequested,
    loadAllCategoriesRequested
})(BookList));
