import './bookForm.scss';
import React, {Component} from "react";
import {Breadcrumb, BreadcrumbItem, Button, Card, CardBody, Col, Container, FormGroup, Row} from "reactstrap";
import {Link, withRouter} from "react-router-dom";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {connect} from "react-redux";
import moment from 'moment';
import {activateAuthLayout} from "../../../store/layout/actions";
import {DatePicker, Select, Spin} from 'antd';
import categoriesAction from '../../../store/categories/action';
import bookAction from '../../../store/books/action';
import ReactQuill from "react-quill";

const {Option} = Select;
const {loadAllCategoriesRequested} = categoriesAction;
const {loadBookByIdRequested, updateBookByIdRequested, createNewBookRequested} = bookAction;

class BookForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            categoriesOptions: [],
            isUpdate: false,
            book: {
                title: '',
                authors: [],
                publisher: '',
                pagesTotal: 0,
                releaseDate: null,
                categories: [],
                status: '',
                body: '',
                cover: '',
                stocks: 0
            }
        };
        this.onFormChanges = this.onFormChanges.bind(this);
        this.onReleaseDateChanges = this.onReleaseDateChanges.bind(this);
        this.onFormSubmitted = this.onFormSubmitted.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.onBodyChanges = this.onBodyChanges.bind(this);
    }

    componentDidMount() {
        this.props.activateAuthLayout();
        const {bookId} = this.props.match.params;
        if (bookId) {
            this.props.loadBookByIdRequested(bookId);
            this.setState({
                isUpdate: true
            })
        }
        this.props.loadAllCategoriesRequested();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.categories !== this.props.categories) {
            const categoriesOption = [];
            this.props.categories.forEach(cat => {
                categoriesOption.push(<Option key={cat.id}>{cat.category}</Option>)
            });
            this.setState({
                categories: this.props.categories,
                categoriesOptions: categoriesOption
            });
        }
        if (prevProps.loadedBook !== this.props.loadedBook) {
            this.setState({
                book: this.props.loadedBook
            })
        }
    }

    // Handle input changes
    onFormChanges(e) {
        e.persist();
        const target = e.target;
        const book = this.state.book;
        book[target.name] = target.value;
        this.setState({
            book: book
        });
    }


    // handle authors select changes
    onAuthorsChange = value => {
        const book = this.state.book;
        book['authors'] = value;
        this.setState({
            book: book
        })
    };

    // handle category select changes
    onCategoryChanges = value => {
        const book = this.state.book;
        book['categories'] = value;
        this.setState({
            book: book
        })
    };

    // handle on status changes
    onStatusChanges = value => {
        const book = this.state.book;
        book['status'] = value;
        this.setState({
            book: book
        })
    };

    // Handle release date changes
    onReleaseDateChanges(date, dateString) {
        const book = this.state.book;
        book['releaseDate'] = dateString;
        this.setState({
            book: book
        })
    }

    // When body changes
    onBodyChanges(value) {
        const book = this.state.book;
        book['body'] = value;
        this.setState({
            book: book
        })
    }

    // cancel update or create book
    onCancel() {
        this.props.history.push('/books/manages/list')
    }

    onFormSubmitted() {
        console.log(this.state.book);
        if (this.state.isUpdate) {
            this.props.updateBookByIdRequested(this.state.book, this.props.history);
        } else {
            this.props.createNewBookRequested(this.state.book, this.props.history);
        }
    }

    render() {
        return (
            <React.Fragment>
                <Container fluid>
                    <div className="page-title-box">
                        <Row className="align-items-center">
                            <Col sm="6">
                                <h4 className="page-title">{this.state.isUpdate ? 'Update Book' : 'Create New Book'}</h4>
                                <Breadcrumb>
                                    <BreadcrumbItem><Link to="/">Rabbit Library</Link></BreadcrumbItem>
                                    <BreadcrumbItem><Link to="/books/manages/list">Books</Link></BreadcrumbItem>
                                    <BreadcrumbItem
                                        active>{this.state.isUpdate ? 'Update Book' : 'Create New Book'}</BreadcrumbItem>
                                </Breadcrumb>
                            </Col>
                        </Row>
                    </div>
                    <Row>
                        {this.props.isLoading &&
                        <Col lg="12" className="d-flex justify-content-center spinner">
                            <Spin size="large"/>
                        </Col>
                        }
                        {!this.props.isLoading && <Col lg="12">
                            <Card>
                                <CardBody>
                                    <AvForm className="form-book-footer">
                                        <Row>
                                            <Col lg="6" md="6" sm="6">
                                                <AvField name="title" label="Book Title"
                                                         placeholder="Book Title" type="text"
                                                         errorMessage="Book title is required"
                                                         value={this.state.book.title}
                                                         onChange={this.onFormChanges}
                                                         validate={{required: {value: true}}}/>
                                            </Col>
                                            <Col lg="6" md="6" sm="6">
                                                <AvField name="publisher" label="Book Publisher"
                                                         value={this.state.book.publisher}
                                                         onChange={this.onFormChanges}
                                                         placeholder="Book Publisher" type="text"
                                                         errorMessage="Book publisher is required"
                                                         validate={{required: {value: true}}}/>
                                            </Col>
                                            <Col lg="6" md="6" sm="6">
                                                <AvField name="cover" label="Book Cover"
                                                         placeholder="Book Cover" type="text"
                                                         value={this.state.book.cover}
                                                         onChange={this.onFormChanges}
                                                         errorMessage="Book cover is required"
                                                         validate={{required: {value: true}}}/>
                                            </Col>
                                            <Col lg="6" md="6" sm="6">
                                                <div className="form-ant-field">
                                                    <label>Authors</label>
                                                    <Select mode="tags" style={{width: '100%'}}
                                                            placeholder="Book Authors"
                                                            value={this.state.book.authors}
                                                            onChange={this.onAuthorsChange}/>
                                                </div>
                                            </Col>
                                            <Col lg="6" md="6" sm="6">
                                                <div className="form-ant-field">
                                                    <label>Categories</label>
                                                    <Select mode="tags" style={{width: '100%'}}
                                                            placeholder="Categories"
                                                            value={this.state.book.categories}
                                                            onChange={this.onCategoryChanges}>
                                                        {this.state.categoriesOptions}
                                                    </Select>
                                                </div>
                                            </Col>
                                            <Col lg="3" md="3" sm="6">
                                                <div className="form-ant-field">
                                                    <label>Release Date</label>
                                                    <DatePicker placeholder="Release Date"
                                                                value={this.state.book.releaseDate ? moment(this.state.book.releaseDate, 'YYYY-MM-DD') : null}
                                                                onChange={this.onReleaseDateChanges}
                                                                style={{width: '100%'}}/>
                                                </div>
                                            </Col>
                                            <Col lg="3" md="3" sm="6">
                                                <div className="form-ant-field">
                                                    <label>Status</label>
                                                    <Select style={{width: '100%'}} placeholder="Status"
                                                            onChange={this.onStatusChanges}
                                                            value={this.state.book.status}>
                                                        <Option value="AVAILABLE">Available</Option>
                                                        <Option value="UPCOMING">Upcoming</Option>
                                                        <Option value="NOT_AVAILABLE">Not Available</Option>
                                                    </Select>
                                                </div>
                                            </Col>
                                            <Col lg="6" md="6" sm="6">
                                                <AvField name="stocks" label="Book Stocks"
                                                         placeholder="Book Stocks" type="number"
                                                         value={this.state.book.stocks}
                                                         onChange={this.onFormChanges}
                                                         errorMessage="Book stocks is required"
                                                         validate={{required: {value: true}}}/>
                                            </Col>
                                            <Col lg="6" md="6" sm="6">
                                                <AvField name="pagesTotal" label="Pages Total"
                                                         value={this.state.book.pagesTotal}
                                                         onChange={this.onFormChanges}
                                                         placeholder="Pages Total" type="number"
                                                         errorMessage="Page totals is required"
                                                         validate={{required: {value: true}}}/>
                                            </Col>
                                            <Col lg="12" className="editor">
                                                <ReactQuill value={this.state.book.body}
                                                            onChange={this.onBodyChanges}/>
                                            </Col>
                                        </Row>
                                        <FormGroup className="mb-0">
                                            <div>
                                                <Button type="button" color="primary"
                                                        onClick={this.onFormSubmitted}
                                                        className="mr-1">
                                                    Submit
                                                </Button>{' '}
                                                <Button type="button"
                                                        onClick={this.onCancel}
                                                        color="secondary">
                                                    Cancel
                                                </Button>
                                            </div>
                                        </FormGroup>
                                    </AvForm>
                                </CardBody>
                            </Card>
                        </Col>}
                    </Row>
                </ Container>
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    const {isLoading, loadedBook} = state.Books;
    const {categories} = state.Categories;
    return {isLoading, loadedBook, categories};
};

export default withRouter(connect(mapStateToProps, {
    activateAuthLayout,
    loadAllCategoriesRequested,
    loadBookByIdRequested, updateBookByIdRequested, createNewBookRequested
})(BookForm));
