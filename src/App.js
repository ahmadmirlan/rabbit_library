import React, {Component, Suspense} from 'react';
import Layout from './components/Layout/';
import {BrowserRouter as Router, Redirect, Route, Switch, withRouter} from 'react-router-dom';
import Loader from './components/Loader/loader';
import Boot from './store/boot.js';
import {connect} from 'react-redux';
import axios from 'axios';
import routes from './routes';
import 'antd/dist/antd.css';
import './custom.css';
import './App.scss';
import {logoutUser} from './store/auth/login/actions';
// Get all Auth methods
import {isUserAuthenticated} from './helpers/authUtils';


function withLayout(WrappedComponent) {
    // ...and returns another component...
    return class extends React.Component {
        render() {
            return <Layout>
                <WrappedComponent></WrappedComponent>
            </Layout>
        }
    };
}

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        axios.interceptors.request.use(function (config) {
            return config
        }, function (error) {
            return Promise.reject(error);
        });

        axios.interceptors.response.use(function (response) {
            return response;
        }, function (error) {
            const errData = error.response.status;
            if (errData === 401) {
                localStorage.removeItem('user');
                localStorage.removeItem('smart_rabbit_token');
                this.props.history.push('/login');
            }
            return Promise.reject(error);
        });
    }

    render() {

        const PrivateRoute = ({component: Component, ...rest}) => (
            <Suspense fallback={<Loader/>}>
                <Route {...rest} render={(props) => (
                    isUserAuthenticated() === true
                        ? <Component {...props} />
                        : <Redirect to='/logout'/>
                )}/>
            </Suspense>
        );

        return (
            <React.Fragment>
                <Router>
                    <Switch>
                        {routes.map((route, idx) =>
                            route.ispublic ?
                                <Route path={route.path} component={route.component} key={idx}/>
                                :
                                <PrivateRoute path={route.path} component={withLayout(route.component)} key={idx}/>
                        )}
                    </Switch>
                </Router>
            </React.Fragment>
        );
    }
}

Boot().then(() => App);

export default withRouter(connect(null, {logoutUser})(App));


