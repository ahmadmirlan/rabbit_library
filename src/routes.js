import {lazy} from 'react';

import Pageslogin2 from './pages/ExtraPages/pages-login-2';
import Pagesregister2 from './pages/ExtraPages/pages-register-2';
import Pagesrecoverpw2 from './pages/ExtraPages/pages-recoverpw-2';
import Pageslockscreen2 from './pages/ExtraPages/pages-lock-screen-2';

import Pageslogin from './pages/Auth/Login';
import Logout from './pages/Auth/Logout';
import Pagesregister from './pages/Auth/Register';
import ForgetPassword from './pages/Auth/ForgetPassword';
import ResetPassword from './pages/Auth/ResetPassword';

const routes = [

    // public Routes
    {path: '/login', component: Pageslogin, ispublic: true},
    {path: '/logout', component: Logout, ispublic: true},
    {path: '/register', component: Pagesregister, ispublic: true},
    {path: '/forget-password', component: ForgetPassword, ispublic: true},
    {path: '/reset-password', component: ResetPassword, ispublic: true},

    // Book routes
    {
        path: '/books/manages/list',
        component: lazy(() => import('./pages/Books/BookList/BookList')),
    },
    {
        path: '/books/manages/add',
        component: lazy(() => import('./pages/Books/BookForm/BookForm')),
    },
    {
        path: '/books/manages/edit/:bookId',
        component: lazy(() => import('./pages/Books/BookForm/BookForm')),
    },

    // Category routes
    {
        path: '/categories/manages/list',
        component: lazy(() => import('./pages/Categories/Categories')),
    },

    // User routes
    {
        path: '/users/manages/list',
        component: lazy(() => import('./pages/Users/UserList/UserList')),
    },
    {
        path: '/roles/manages/list',
        component: lazy(() => import('./pages/Roles/Roles')),
    },

    {path: '/pages-login-2', component: Pageslogin2},
    {path: '/pages-register-2', component: Pagesregister2},
    {path: '/pages-recoverpw-2', component: Pagesrecoverpw2},
    {path: '/pages-lock-screen-2', component: Pageslockscreen2},

    // Dashboard
    {path: '/dashboard', component: lazy(() => import('./pages/Home/Home')), exact: true},
    {path: '/', component: lazy(() => import('./pages/Home/Home'))},

];

export default routes;
