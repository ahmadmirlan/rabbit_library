import {
    createNewBook,
    getAllBookAdminMode,
    getBookById,
    loadPublishedBook,
    updateBookById
} from '../../helpers/bookHelper';
import {all, call, put, takeEvery} from 'redux-saga/effects';
import action from './action';
import {notification} from 'antd';

function* loadAllBookAdvanced({payload}) {
    try {
        yield put(action.loadBookPageToggleLoading(true));
        const response = yield call(getAllBookAdminMode, payload.query);
        yield put(action.loadAllBookAdvancedSuccess(response.data, response.page, payload.query));
        yield put(action.loadBookPageToggleLoading(false));
    } catch (error) {
        yield put(action.loadAllBookAdvancedError(error));
        yield put(action.loadBookPageToggleLoading(false));
    }
}

function* loadPublishedBookWatch({payload}) {
    try {
        yield put(action.loadBookPageToggleLoading(true));
        const response = yield call(loadPublishedBook, payload.query);
        yield put(action.loadPublishedBookSuccess(response.data, response.page, payload.query));
        yield put(action.loadBookPageToggleLoading(false));
    } catch (e) {
        yield put(action.loadBookPageToggleLoading(false));
    }
}

function* loadBookById({payload}) {
    /*const selector = useSelector();
    const {books} = selector(state => state.Books);*/
    try {
        yield put(action.loadBookPageToggleLoading(true));
        let book = undefined;
        /*if (books) {
            book = books.find(book => {
                return book.id === payload.bookId
            });
        }*/

        if (book) {
            yield put(action.loadBookByIdSuccess(book));
        } else {
            const response = yield call(getBookById, payload.bookId);
            yield put(action.loadBookByIdSuccess(response));
        }
        yield put(action.loadBookPageToggleLoading(false));
    } catch (e) {
        yield put(action.loadBookPageToggleLoading(false));
    }
}

function* updateBookByIdWatch({payload}) {
    try {
        yield put(action.loadBookPageActionLoading(true));
        const response = yield call(updateBookById, payload.book);
        yield put(action.updateBookByIdSuccess(response));
        yield put(action.loadBookPageActionLoading(false));
        notification['success']({
            message: 'Success',
            description:
                `Book ${response.title} updated successfully!`,
        });
        payload.history.push('/books/manages/list');
    } catch (e) {
        yield put(action.loadBookPageActionLoading(false));
        notification['error']({
            message: 'Failed',
            description:
                `Failed to updated ${payload.book.title}!`,
        });
    }
}

function* createNewBookWatch({payload}) {
    try {
        yield put(action.loadBookPageActionLoading(true));
        const response = yield call(createNewBook, payload.book);
        yield put(action.createNewBookSuccess(response));
        yield put(action.loadBookPageActionLoading(false));
    } catch (e) {
        yield put(action.loadBookPageActionLoading(false));
    }
}

export default function* rootSaga() {
    yield all([
        takeEvery(action.LOAD_ALL_BOOK_ADVANCED_REQUESTED, loadAllBookAdvanced),
        takeEvery(action.LOAD_BOOK_BY_ID_REQUESTED, loadBookById),
        takeEvery(action.UPDATE_BOOK_BY_ID_REQUESTED, updateBookByIdWatch),
        takeEvery(action.CREATE_NEW_BOOK_REQUESTED, createNewBookWatch),
        takeEvery(action.LOAD_PUBLISHED_BOOK_REQUESTED, loadPublishedBookWatch),
    ])
}
