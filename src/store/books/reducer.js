import action from './action.js';

const initBookState = {
    isLoading: false,
    errorMessage: false,
    books: [],
    pages: {
        size: 0,
        totalElements: 0,
        totalPage: 0,
        page: 0
    },
    modalActive: false,
    lastQuery: {
        filter: {},
        pageNumber: 0,
        pageSize: 10,
        sortField: "createdAt",
        sortOrder: "desc"
    },
    loadedBook: {
        id: '',
        title: '',
        publisher: '',
        body: '',
        status: 'READY', // publish
        authors: '',
        created_at: '',
        updated_at: '',
    },
};

export default function bookReducer(
    state = initBookState,
    {type, payload}
) {
    switch (type) {
        case action.LOAD_ALL_BOOK_ADVANCED_REQUESTED:
            return {
                ...state,
                isLoading: true,
                errorMessage: false,
                modalActive: false,
            };
        case action.LOAD_ALL_BOOK_ADVANCED_SUCCESS:
            return {
                ...state,
                isLoading: false,
                books: payload.items,
                pages: payload.pages,
                lastQuery: payload.lastQuery,
                errorMessage: false,
            };
        case action.LOAD_ALL_BOOK_ADVANCED_ERROR:
            return {
                ...state,
                isLoading: false,
                errorMessage: 'There is a loading problem',
            };
        case action.LOAD_BOOK_BY_ID_SUCCESS:
            return {
                ...state,
                loadedBook: payload.book
            };
        case action.LOAD_BOOK_PAGE_TOGGLE_LOADING:
            return {
                ...state,
                isLoading: payload.isLoading
            };
        case action.LOAD_BOOK_PAGE_ACTION_LOADING:
            return {
                ...state,
                isLoading: payload.isLoading
            };
        case action.UPDATE_BOOK_BY_ID_SUCCESS:
            return {
                ...state,
                loadedBook: payload.book
            };
        case action.CREATE_NEW_BOOK_SUCCESS:
            return {
                ...state,
                loadedBook: payload.book
            };
        case action.LOAD_PUBLISHED_BOOK_SUCCESS:
            return {
                ...state,
                books: payload.items,
                pages: payload.pages,
                lastQuery: payload.query
            };
        default:
            return {
                ...state,
            };
    }
}
