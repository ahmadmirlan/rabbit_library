const action = {
    LOAD_ALL_BOOK_ADVANCED_REQUESTED: '[Book] Load All Book Advanced Requested',
    LOAD_ALL_BOOK_ADVANCED_SUCCESS: '[Book] Load All Book Advanced Success',
    LOAD_ALL_BOOK_ADVANCED_ERROR: '[Book] Load All Book Advanced Error',
    LOAD_BOOK_BY_ID_REQUESTED: '[Book] Load Book By Id Requested',
    LOAD_BOOK_BY_ID_SUCCESS: '[Book] Load Book By Id Success',
    LOAD_BOOK_PAGE_TOGGLE_LOADING: '[Load Book] Load Book Toggle Loading',
    LOAD_BOOK_PAGE_ACTION_LOADING: '[Load Book] Load Book Action Loading',
    UPDATE_BOOK_BY_ID_REQUESTED: '[Update Book] Update Book By id Requested',
    UPDATE_BOOK_BY_ID_SUCCESS: '[Update Book] Update Book By id Success',
    CREATE_NEW_BOOK_REQUESTED: '[Create New Book] Create New Book Requested',
    CREATE_NEW_BOOK_SUCCESS: '[Create New Book] Create New Book Success',
    LOAD_PUBLISHED_BOOK_REQUESTED: '[Published Book] Load Published Book Requested',
    LOAD_PUBLISHED_BOOK_SUCCESS: '[Published Book] Load Published Book Success',
    // Request all book advanced
    loadAllBookAdvancedRequested: (query) => {
        return {
            type: action.LOAD_ALL_BOOK_ADVANCED_REQUESTED,
            payload: {query}
        }
    },

    //all book advanced loaded success
    loadAllBookAdvancedSuccess: (items, pages, lastQuery) => {
        return {
            type: action.LOAD_ALL_BOOK_ADVANCED_SUCCESS,
            payload: {items, pages, lastQuery}
        }
    },

    loadAllBookAdvancedError: (error) => {
        return {
            type: action.LOAD_ALL_BOOK_ADVANCED_ERROR,
            payload: {error}
        }
    },

    loadBookByIdRequested: (bookId) => {
        return {
            type: action.LOAD_BOOK_BY_ID_REQUESTED,
            payload: {bookId}
        }
    },

    loadBookByIdSuccess: (book) => {
        return {
            type: action.LOAD_BOOK_BY_ID_SUCCESS,
            payload: {book}
        }
    },

    loadBookPageToggleLoading: (isLoading) => {
        return {
            type: action.LOAD_BOOK_PAGE_TOGGLE_LOADING,
            payload: {isLoading}
        }
    },

    loadBookPageActionLoading: (isLoading) => {
        return {
            type: action.LOAD_BOOK_PAGE_ACTION_LOADING,
            payload: {isLoading}
        }
    },

    updateBookByIdRequested: (book, history) => {
        return {
            type: action.UPDATE_BOOK_BY_ID_REQUESTED,
            payload: {book, history}
        }
    },
    updateBookByIdSuccess: (book) => {
        return {
            type: action.UPDATE_BOOK_BY_ID_SUCCESS,
            payload: {book}
        }
    },
    createNewBookRequested: (book, history) => {
        return {
            type: action.CREATE_NEW_BOOK_REQUESTED,
            payload: {book, history}
        }
    },
    createNewBookSuccess: (book) => {
        return {
            type: action.CREATE_NEW_BOOK_SUCCESS,
            payload: {book}
        }
    },
    loadPublishedBookRequested: (query) => {
        return {
            type: action.LOAD_PUBLISHED_BOOK_REQUESTED,
            payload: {query}
        }
    },
    loadPublishedBookSuccess: (items, pages, query) => {
        return {
            type: action.LOAD_PUBLISHED_BOOK_SUCCESS,
            payload: {items, pages, query}
        }
    }
};

export default action;
