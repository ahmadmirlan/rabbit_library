const categoriesAction = {
    LOAD_ALL_CATEGORIES_REQUESTED: '[Categories] Load All Categories Requested',
    LOAD_ALL_CATEGORIES_SUCCESS: '[Categories] Load All Categories Success',
    CATEGORY_PAGE_ACTION_LOADING: '[Category] Category Page Action Loading',
    CATEGORY_PAGE_TOGGLE_LOADING: '[Category] Category Page Toggle Loading',

    // Load all categories requested
    loadAllCategoriesRequested: () => {
        return {
            type: categoriesAction.LOAD_ALL_CATEGORIES_REQUESTED
        }
    },

    // After categories load success
    loadAllCategoriesSuccess: (items) => {
        return {
            type: categoriesAction.LOAD_ALL_CATEGORIES_SUCCESS,
            payload: {items}
        }
    },

    // Define loading when do action (create, update or delete)
    categoryPageActionLoading: (loading) => {
        return {
            type: categoriesAction.CATEGORY_PAGE_ACTION_LOADING,
            payload: {loading}
        }
    },

    // Define loading when request get data
    categoryPageToggleLoading: (loading) => {
        return {
            type: categoriesAction.CATEGORY_PAGE_TOGGLE_LOADING,
            payload: {loading}
        }
    }
};

export default categoriesAction;
