import {all, call, put, takeEvery} from 'redux-saga/effects';
import {getAllCategories} from '../../helpers/categoryHelper';
import categoriesAction from './action';

function* loadAllCategories() {
    try {
        yield put(categoriesAction.categoryPageToggleLoading(true));
        const response = yield call(getAllCategories);
        yield put(categoriesAction.loadAllCategoriesSuccess(response.data));
        yield put(categoriesAction.categoryPageToggleLoading(false));
    } catch (e) {
        console.log(e);
        yield put(categoriesAction.categoryPageToggleLoading(false));
    }
}

export default function* rootSaga() {
    yield all([
        takeEvery(categoriesAction.LOAD_ALL_CATEGORIES_REQUESTED, loadAllCategories)
    ])
}
