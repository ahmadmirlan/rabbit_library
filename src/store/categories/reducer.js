import categoriesAction from "./action";

const initCategoryState = {
    isLoading: false,
    errorMessage: false,
    categories: [],
    modalActive: false,
    category: {},
};

export default function categoryReducer(state = initCategoryState, {type, payload}) {
    switch (type) {
        case categoriesAction.CATEGORY_PAGE_TOGGLE_LOADING:
            return {
                ...state,
                isLoading: payload.loading
            };
        case categoriesAction.CATEGORY_PAGE_ACTION_LOADING:
            return {
                ...state,
                isLoading: payload.loading
            };
        case categoriesAction.LOAD_ALL_CATEGORIES_SUCCESS:
            return {
                ...state,
                isLoading: false,
                categories: payload.items,
                errorMessage: false,
                modalActive: false
            };
        default:
            return {
                ...state
            }
    }
}
