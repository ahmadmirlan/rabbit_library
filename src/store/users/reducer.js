import userAction from './action';

const initUserState = {
    isLoading: false,
    users: [],
    loggedUser: {},
    pages: {
        size: 0,
        totalElements: 0,
        totalPage: 0,
        page: 0
    },
    lastQuery: {
        filter: {},
        pageNumber: 0,
        pageSize: 10,
        sortField: "createdAt",
        sortOrder: "desc"
    },
    loadedUser: {},
};

export default function userReducer(
    state = initUserState,
    {type, payload}
) {
    switch (type) {
        case userAction.LOAD_USER_PAGE_ACTION_LOADING:
            return {
                ...state,
                isLoading: payload.isLoading
            };
        case userAction.LOAD_USER_PAGE_TOGGLE_LOADING:
            return {
                ...state,
                isLoading: payload.isLoading
            };
        case userAction.LOAD_ALL_USERS_SUCCESS:
            return {
                ...state,
                pages: payload.pages,
                users: payload.items,
                lastQuery: payload.lastQuery
            };
        case userAction.LOAD_USER_BY_TOKEN_SUCCESS:
            return {
                ...state,
                loggedUser: payload.user
            };
        default:
            return {
                ...state,
            }
    }
}
