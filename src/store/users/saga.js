import userAction from './action';
import {all, call, put, takeEvery} from 'redux-saga/effects';
import {getAllUsers, loadUserByToken} from '../../helpers/usersHelper';

function* loadAllUsersWatch({payload}) {
    try {
        yield put(userAction.loadUserPageToggleLoading(true));
        const response = yield call(getAllUsers, payload.query);
        yield put(userAction.loadAllUserSuccess(response.data, response.page, payload.query));
        yield put(userAction.loadUserPageToggleLoading(false));
    } catch (e) {
        yield put(userAction.loadUserPageToggleLoading(false));
    }
}

function* loadUserByTokenWatch({payload}) {
    try {
        yield put(userAction.loadUserPageToggleLoading(true));
        const response = yield call(loadUserByToken, payload.token);
        yield put(userAction.loadUserByTokenSuccess(response));
        yield put(userAction.loadUserPageToggleLoading(false));
    }catch (e) {
        yield put(userAction.loadUserPageToggleLoading(false));
    }
}

export default function* rootSaga() {
    yield all([
        takeEvery(userAction.LOAD_ALL_USERS_REQUESTED, loadAllUsersWatch),
        takeEvery(userAction.LOAD_USER_BY_TOKEN_REQUESTED, loadUserByTokenWatch)
    ])
}
