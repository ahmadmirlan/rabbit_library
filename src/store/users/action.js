const userAction = {
    LOAD_ALL_USERS_REQUESTED: '[Users] Load All Users Requested',
    LOAD_ALL_USERS_SUCCESS: '[Users] Load All Users SUCCESS',
    LOAD_USER_PAGE_TOGGLE_LOADING: '[Users Toggle Loading] Load User Page Toggle Loading',
    LOAD_USER_PAGE_ACTION_LOADING: '[Users Action Loading] Load User Page Action Loading',
    LOAD_USER_BY_TOKEN_REQUESTED: '[User] Load User By Token Requested',
    LOAD_USER_BY_TOKEN_SUCCESS: '[User] Load User By Token Success',

    loadUserPageToggleLoading: (isLoading) => {
        return {
            type: userAction.LOAD_USER_PAGE_TOGGLE_LOADING,
            payload: {isLoading}
        }
    },

    loadUserPageActionLoading: (isLoading) => {
        return {
            type: userAction.LOAD_USER_PAGE_ACTION_LOADING,
            payload: {isLoading}
        }
    },

    loadAllUseRequested: (query) => {
        return {
            type: userAction.LOAD_ALL_USERS_REQUESTED,
            payload: {query}
        }
    },

    loadAllUserSuccess: (items, pages, lastQuery) => {
        return {
            type: userAction.LOAD_ALL_USERS_SUCCESS,
            payload: {items, pages, lastQuery}
        }
    },

    loadUserByTokenRequested: (token) => {
        return {
            type: userAction.LOAD_USER_BY_TOKEN_REQUESTED,
            payload: {token}
        }
    },

    loadUserByTokenSuccess: (user) => {
        return {
            type: userAction.LOAD_USER_BY_TOKEN_SUCCESS,
            payload: {user}
        }
    }
};

export default userAction;
