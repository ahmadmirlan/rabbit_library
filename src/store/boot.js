import store from './index';
import userAction from './users/action';
import roleAction from "./auth/roles/action";
import {getActiveToken} from '../helpers/authUtils';

export default () =>
    new Promise(() => {
        store.dispatch(userAction.loadUserByTokenRequested(getActiveToken()));
        store.dispatch(roleAction.loadAllPermissionsRequested());
        store.dispatch(roleAction.loadAllRolesRequested());
    });
