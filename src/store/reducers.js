import { combineReducers  } from 'redux';

// Front
import Layout from './layout/reducer';

// Authentication Module
import Account from './auth/register/reducer';
import Login from './auth/login/reducer';
import Forget from './auth/forgetpwd/reducer';

import Books from './books/reducer';
import Categories from './categories/reducer';
import Users from './users/reducer';
import Roles from './auth/roles/reducer';
const rootReducer = combineReducers({

    // public
    Layout,

    // Authentication
    Account,
    Login,
    Forget,
    Books,
    Categories,
    Users,
    Roles

});

export default rootReducer;
