import { all } from 'redux-saga/effects'

//public
import accountSaga from './auth/register/saga';
import loginSaga from './auth/login/saga';
import forgetSaga from './auth/forgetpwd/saga';
import categoriesSaga from './categories/saga';
import booksSaga from './books/saga';
import usersSaga from './users/saga';
import rolesSaga from './auth/roles/saga';

export default function* rootSaga() {
    yield all([

        //public
        accountSaga(),
        loginSaga(),
        forgetSaga(),
        categoriesSaga(),
        booksSaga(),
        usersSaga(),
        rolesSaga()
    ])
}
