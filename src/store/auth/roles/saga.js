import {all, call, put, takeEvery} from 'redux-saga/effects';
import {getAllPermissions, getAllRoles} from '../../../helpers/authUtils';
import roleAction from "./action";

function* loadAllPermissionsWatch() {
    try {
        const response = yield call(getAllPermissions);
        yield put(roleAction.loadAllPermissionsSuccess(response));
    } catch (e) {
        console.log(e);
    }
}

function* loadAllRolesWatch() {
    try {
        const response = yield call(getAllRoles);
        yield put(roleAction.loadAllRolesSuccess(response));
    } catch (e) {
        console.log(e);
    }
}

export default function* rootSaga() {
    yield all([
        takeEvery(roleAction.LOAD_ALL_PERMISSIONS_REQUESTED, loadAllPermissionsWatch),
        takeEvery(roleAction.LOAD_ALL_ROLES_REQUESTED, loadAllRolesWatch)
    ])
}
