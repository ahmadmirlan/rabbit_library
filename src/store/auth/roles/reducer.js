import roleAction from "./action";

const initRolesState = {
    isLoading: false,
    roles: [],
    permissions: [],
    loadedRole: {},
    loadedPermission: {}
};

export default function rolesReducer(state = initRolesState,
                                     {type, payload}) {
    switch (type) {
        case roleAction.LOAD_ROLE_PAGE_TOGGLE_LOADING:
            return {
                ...state,
                isLoading: payload.isLoading
            };
        case roleAction.LOAD_ROLE_PAGE_ACTION_LOADING:
            return {
                ...state,
                isLoading: payload.isLoading
            };
        case roleAction.LOAD_ALL_ROLES_SUCCESS:
            return {
                ...state,
                roles: payload.items
            };
        case roleAction.LOAD_ALL_PERMISSIONS_SUCCESS:
            return {
                ...state,
                permissions: payload.items
            };
        default:
            return {
                ...state
            }
    }

}
