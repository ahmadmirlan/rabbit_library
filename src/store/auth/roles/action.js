const roleAction = {
    LOAD_ROLE_PAGE_TOGGLE_LOADING: '[Load Role/Permission] Load Data Toggle Loading',
    LOAD_ROLE_PAGE_ACTION_LOADING: '[Load Role/Permission] Load Data Action Loading',
    LOAD_ALL_PERMISSIONS_REQUESTED: '[Permissions] Load All Permissions Requested',
    LOAD_ALL_PERMISSIONS_SUCCESS: '[Permissions] Load All Permissions Success',
    LOAD_ALL_ROLES_REQUESTED: '[Roles] Load All Role Requested',
    LOAD_ALL_ROLES_SUCCESS: '[Roles] Load All Role Success',

    loadDataPageToggleLoading: (isLoading) => {
        return {
            type: roleAction.LOAD_ROLE_PAGE_TOGGLE_LOADING,
            payload: {isLoading}
        }
    },

    loadDataPageActionLoading: (isLoading) => {
        return {
            type: roleAction.LOAD_ROLE_PAGE_ACTION_LOADING,
            payload: {isLoading}
        }
    },

    loadAllPermissionsRequested: () => {
        return {
            type: roleAction.LOAD_ALL_PERMISSIONS_REQUESTED
        }
    },

    loadAllPermissionsSuccess: (items) => {
        return {
            type: roleAction.LOAD_ALL_PERMISSIONS_SUCCESS,
            payload: {items}
        }
    },

    loadAllRolesRequested: () => {
        return {
            type: roleAction.LOAD_ALL_ROLES_REQUESTED
        }
    },

    loadAllRolesSuccess: (items) => {
        return {
            type: roleAction.LOAD_ALL_ROLES_SUCCESS,
            payload: {items}
        }
    },
};

export default roleAction;
