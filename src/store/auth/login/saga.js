import {all, call, fork, put, takeEvery} from 'redux-saga/effects';
// Login Redux States
import {CHECK_LOGIN, LOGOUT} from './actionTypes';
import {apiError, loginUserSuccessful} from './actions';
import userAction from '../../users/action';
// AUTH related methods
import {postLogin, setLoggeedInUser} from '../../../helpers/authUtils';

//If user is login then dispatch redux action's are directly from here.
function* loginUser({payload: {username, password, history}}) {
    try {
        const response = yield call(postLogin, {email: username, password: password});
        setLoggeedInUser(response);
        yield put(loginUserSuccessful(response));
        yield put(userAction.loadUserByTokenRequested(''));
        history.push('/dashboard');
    } catch (error) {
        yield put(apiError(error));
    }
}

function logoutUser({payload}) {
    payload.history.push('/login');
}

export function* watchUserLogin() {
    yield takeEvery(CHECK_LOGIN, loginUser)
}

function* loginSaga() {
    yield all([fork(watchUserLogin), takeEvery(LOGOUT, logoutUser)]);
}

export default loginSaga;
