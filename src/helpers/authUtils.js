import axios from 'axios';

//Set the logged in user data in local session 
const setLoggeedInUser = (user) => {
    localStorage.setItem('user', JSON.stringify(user));
    localStorage.setItem('smart_rabbit_token', JSON.stringify(user.accessToken));
};

// Gets the logged in user data from local session 
const getLoggedInUser = () => {
    const user = localStorage.getItem('user');
    if (user)
        return JSON.parse(user);
    return null;
};

const getActiveToken = () => {
    const token = localStorage.getItem('smart_rabbit_token');
    if (token) {
        return JSON.parse(token);
    }
    return null;
};

//is user is logged in
const isUserAuthenticated = () => {
    return getActiveToken() !== null && getActiveToken() !== undefined;
};

// Register Method
const postRegister = (url, data) => {
    return axios.post(url, data).then(response => {
        if (response.status >= 200 || response.status <= 299)
            return response.data;
        throw response.data;
    }).catch(err => {
        var message;
        if (err.response && err.response.status) {
            switch (err.response.status) {
                case 404:
                    message = "Sorry! the page you are looking for could not be found";
                    break;
                case 500:
                    message = "Sorry! something went wrong, please contact our support team";
                    break;
                case 401:
                    message = "Invalid credentials";
                    break;
                default:
                    message = err[1];
                    break;
            }
        }
        throw message;
    });

};

// Login Method
const postLogin = (data) => {
    return axios.post('/api/login', data).then(response => {
        if (response.status === 400 || response.status === 500)
            throw response.data;
        return response.data.user;
    }).catch(err => {
        throw err[1];
    });
};

// postForgetPwd 
const postForgetPwd = (url, data) => {
    return axios.post(url, data).then(response => {
        if (response.status === 400 || response.status === 500)
            throw response.data;
        return response.data;
    }).catch(err => {
        throw err[1];
    });
};

// define headers
const defineHeaders = () => {
    const token = getActiveToken();
    return {
        headers: {'Authorization': token}
    };
};

const getAllRoles = () => {
    return axios.get('/api/roles', defineHeaders()).then(response => {
        return response.data.data;
    })
};

const getAllPermissions = () => {
    return axios.get('/api/permissions', defineHeaders()).then(response => {
        return response.data.data;
    })
};

export {
    setLoggeedInUser, getLoggedInUser, isUserAuthenticated, postRegister, postLogin,
    postForgetPwd, defineHeaders, getActiveToken, getAllRoles, getAllPermissions
}
