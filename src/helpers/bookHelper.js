import axios from 'axios';
import {defineHeaders} from "./authUtils";

const body = {
    "filter": {},
    "pageNumber": 0,
    "pageSize": 20,
    "sortField": "title",
    "sortOrder": "desc"
};

export function getAllBookAdminMode(query) {
    return axios.post('/api/books/find/advanced', query, defineHeaders()).then(response => {
        return response.data;
    });
}

export function getBookById(bookId) {
    return axios.get(`/api/books/find/advanced/book/${bookId}`, defineHeaders()).then(response => {
        return response.data;
    });
}

export function updateBookById(book) {
    return axios.put(`/api/books/update/book/${book.id}`, book, defineHeaders()).then(response => {
        return response.data;
    })
}

export function createNewBook(book) {
    return axios.post(`/api/books/create/book`, book, defineHeaders()).then(response => {
        return response.data;
    })
}

export function loadPublishedBook(query) {
    return axios.post('/api/books/allPublished', query, defineHeaders()).then(response => {
        console.log(response.data);
        return response.data
    });
}
