import axios from 'axios';
import {defineHeaders} from "./authUtils";
export function getAllCategories() {
    return axios.get('/api/categories', defineHeaders()).then(response => {
        return response.data;
    })
}
