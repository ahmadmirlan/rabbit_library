import axios from 'axios';
import {defineHeaders} from "./authUtils";

export function getAllUsers(query) {
    return axios.post('/api/users/findUsers', query, defineHeaders()).then(response => {
        return response.data;
    })
}

export function loadUserByToken(token) {
    return axios.get('/api/users/find/byToken/', defineHeaders()).then(response => {
        return response.data;
    })
}
