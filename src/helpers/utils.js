export const truncateHTML = (text, length) => {
    const charLimit = length;
    const withoutHtml = text.replace(/<(?:.|\n)*?>/gm, '');
    if (!text || text.length <= charLimit) {
        return withoutHtml;
    }
    return withoutHtml.substring(0, charLimit) + '...';
};
