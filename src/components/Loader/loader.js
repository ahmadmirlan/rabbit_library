import React from 'react';
import LoaderComponent from './loader.style';
import {Spin} from 'antd';
export default Loader => (
  <LoaderComponent>
    <Spin size="large"/>
  </LoaderComponent>
);
