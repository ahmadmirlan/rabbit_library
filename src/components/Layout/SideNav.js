import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class SideNav extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <React.Fragment>
                <div id="sidebar-menu">
                    <ul className="metismenu" id="menu">
                        <li className="menu-title">Main</li>
                        <li>
                            <Link to="/dashboard" className="waves-effect">
                                <i className="ti-home"></i> <span> Dashboard </span>
                            </Link>
                        </li>

                        <li className="menu-title">Manages</li>
                        {/*Book pages*/}
                        <li>
                            <Link to="/#" className="waves-effect"><i className="ti-book"></i> <span> Books <span className="float-right menu-arrow"><i className="mdi mdi-chevron-right"></i></span> </span> </Link>
                            <ul className="submenu">
                                <li><Link to="/books/manages/list">Book List</Link></li>
                                <li><Link to="/books/manages/add">Add New Book</Link></li>
                            </ul>
                        </li>
                        <li>
                            <Link to="/categories/manages/list" className="waves-effect">
                                <i className="ti-list"></i><span> Categories </span>
                            </Link>
                        </li>

                        <li className="menu-title">Users</li>
                        <li>
                            <Link to="/users/manages/list" className="waves-effect">
                                <i className="ti-user"></i><span> Users </span>
                            </Link>
                        </li>
                        <li>
                            <Link to="/roles/manages/list" className="waves-effect">
                                <i className="ti-panel"></i><span> Roles </span>
                            </Link>
                        </li>
                    </ul>
                </div>

            </React.Fragment>
        );
    }
}


export default SideNav;
