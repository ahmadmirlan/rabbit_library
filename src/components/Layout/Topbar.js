import React, { Component } from 'react';
import LanguageMenu from './Menus/languageMenu';
import { Link } from 'react-router-dom';
import NotificationMenu from './Menus/notificationMenu';
import ProfileMenu from './Menus/profileMenu';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import {withRouter} from 'react-router-dom';
import { isLarge } from '../../store/actions';
import { connect } from 'react-redux';

import logoLight from "../../images/logo-light.png";
import logoSmall from "../../images/logo-sm.png";

class Topbar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            create_menu: false,
            toggle: false,
        };
        this.toggleCreate = this.toggleCreate.bind(this);
        this.quickAction = this.quickAction.bind(this);
    }
    toggleCreate() {
        this.setState(prevState => ({
            create_menu: !prevState.create_menu
        }));
    }

    sidebarToggle = () => {
        document.body.classList.toggle('enlarged');
        this.props.isLarge(!this.props.is_large_state);
    };

    toggleFullscreen() {
        if (!document.fullscreenElement && /* alternative standard method */ !document.mozFullScreenElement && !document.webkitFullscreenElement) {  // current working methods
            if (document.documentElement.requestFullscreen) {
                document.documentElement.requestFullscreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullscreen) {
                document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    }

    quickAction(url) {
        this.props.history.push(url)
    }

    render() {
        return (
            <React.Fragment>
                <div className="topbar">
                    <div className="topbar-left">
                        <Link to="/" className="logo">
                            <span>
                                <img src={logoLight} alt="" height="18" />
                            </span>
                            <i>
                                <img src={logoSmall} alt="" height="22" />
                            </i>
                        </Link>
                    </div>

                    <nav className="navbar-custom">
                        <ul className="navbar-right list-inline float-right mb-0">
                            <LanguageMenu />

                            <li className="dropdown notification-list list-inline-item d-none d-md-inline-block mr-1">
                                <Link onClick={this.toggleFullscreen} className="nav-link waves-effect" to="#" id="btn-fullscreen">
                                    <i className="mdi mdi-fullscreen noti-icon"></i>
                                </Link>
                            </li>

                            <NotificationMenu />
                            <ProfileMenu />
                        </ul>
                        <ul className="list-inline menu-left mb-0">
                            <li className="float-left">
                                <button onClick={this.sidebarToggle} className="button-menu-mobile open-left waves-effect">
                                    <i className="mdi mdi-menu"></i>
                                </button>
                            </li>

                            <li className="d-none d-sm-block">
                                <Dropdown isOpen={this.state.create_menu} toggle={this.toggleCreate} className="pt-3 d-inline-block">
                                    <DropdownToggle className="btn btn-light" caret tag="a">
                                        Quick Access {' '}{' '}{' '}
                                    </DropdownToggle>
                                    <DropdownMenu >
                                        <DropdownItem tag="a" onClick={() => this.quickAction('/books/manages/add')}>Add Book</DropdownItem>
                                        <DropdownItem tag="a" onClick={() => this.quickAction('/categories/manages/list')}>Add Category</DropdownItem>
                                    </DropdownMenu>
                                </Dropdown>
                            </li>
                        </ul>
                    </nav>
                </div>
            </React.Fragment>
        );
    }
}

const mapStatetoProps = state => {
    const { is_large_state } = state.Layout;
    const { loggedUser } = state.Users;
    return { is_large_state, loggedUser };
};


export default withRouter(connect(mapStatetoProps, { isLarge })(Topbar));
